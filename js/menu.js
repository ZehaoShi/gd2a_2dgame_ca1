function menuClickSound(){
    var clicksound = document.getElementById("ClickSound");
    clicksound.play();
}

function hoverOnButton(){
    var hoverOnButton = document.getElementById("hoverOnButton");
    hoverOnButton.play();
}

function hoverOnHelpButton() {
    var hoverOnHelpButton = document.getElementById("hoverOnHelpButton");
    var score = document.getElementById("score");
    hoverOnHelpButton.play(score);
    hoverOnHelpButton.classList.toggle("show");
}

function ScoreBoard(){ // menu display score board and display div when player click scoreboard button
    var ScoreBoard = document.getElementById("ScoreBoard");
    var CloseWindow = document.getElementById("CloseWindow");
    ScoreBoard.style.display = "block";
    CloseWindow.style.display = "block";
    for(var i=0; i<localStorage.playerdata.length; i++){
        ScoreBoard.innerHTML = 'Player: ' + localStorage.playerdata;
    }
}

function SaveUser(){ // save username
    var username = document.getElementById("username").value;
    if(username != null){
        localStorage.setItem("user", username);
    }
    if(username == null)
    {
        localStorage.setItem("user", "Unnamed");
    }
}

function closeit(){ // close the pop up div
    var ScoreBoard = document.getElementById("ScoreBoard");
    var CloseWindow = document.getElementById("CloseWindow");
    ScoreBoard.style.display = "none";
    helppage.style.display = "none";
    CloseWindow.style.display = "none";
}

function help(){ // open the help page
    var helppage = document.getElementById("helppage");
    var CloseWindow = document.getElementById("CloseWindow");
    helppage.style.display = "block";
    CloseWindow.style.display = "block";
}

function changelevel(){ // change game difficulty
    var Difficulty = document.getElementById("Difficulty");
    
    if(Difficulty.innerHTML == "Easy"){
        Difficulty.innerHTML = "Normal";
        Difficulty.style.backgroundColor = "rgb(255,140,0,0.7)";
        localStorage.setItem("level", 2);
        localStorage.setItem("difficulty", "Normal");
    }
    else if(Difficulty.innerHTML == "Normal"){
        Difficulty.innerHTML = "Hard";
        Difficulty.style.backgroundColor = "rgb(255,69,0,0.7)";
        localStorage.setItem("level", 3);
        localStorage.setItem("difficulty", "Hard");
    }
    else if(Difficulty.innerHTML == "Hard"){
        Difficulty.innerHTML = "Insane";
        Difficulty.style.backgroundColor = "rgb(255,0,0, 0.8)";
        localStorage.setItem("level", 4);
        localStorage.setItem("difficulty", "Insane");
    } 

    else if(Difficulty.innerHTML == "Insane"){
        Difficulty.innerHTML = "Easy";
        Difficulty.style.backgroundColor = "rgb(173,255,47,0.8)";
        localStorage.setItem("level", 1);
        localStorage.setItem("difficulty", "Easy");
    } 
}

function setDefaultDifficulty(){ // set default game difficulty
    localStorage.setItem("level", 1); // default Game Difficulty
}
