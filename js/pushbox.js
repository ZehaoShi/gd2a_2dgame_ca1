var pushbox = function () {
    // Get div from html
    this.canvas = document.getElementById('pushbox-canvas'), // get canvas element
    this.context = this.canvas.getContext('2d'), // 2d context
    this.fpsElement = document.getElementById('fps'),
    this.musicElement = document.getElementById('music'),
    this.musicCheckboxElement = document.getElementById('music-checkbox'),
    this.lose = document.getElementById('lose'),
    this.win = document.getElementById('win'),
    this.Bestscore = document.getElementById('Bestscore'),
    this.CurrentScore = document.getElementById('CurrentScore'),
    this.playerdetails = document.getElementById('playerdetails'),
    this.pauseText = document.getElementById("pauseText"),
    this.musicOn = this.musicCheckboxElement.checked, 

    //Constants
    this.lastAnimationFrameTime = 0,
    this.lastFpsUpdateTime = 0,
    this.fps = 60,

    this.background = new Image(),
    this.runnerImage = new Image();
    this.spriteSheet = new Image();

    //KEYS
    this.A_KEY = 65,
    this.LEFT_ARROW = 37,
    this.D_KEY = 68,
    this.RIGHT_ARROW = 39,
    this.W_KEY = 87,
    this.UP_ARROW = 38,
    this.S_KEY = 83,
    this.DOWN_ARROW = 40,
    this.P_KEY = 80,

    //status
    this.paused = false,
    this.pauseStartTime = 0,
    this.PAUSE_CHECK_INTERVAL = 200, // milliseconds

    this.gameStarted = false,

    //player movement
    this.tickX = 12,
    this.tickY = 12,
    this.keyA = false,
    this.keyW = false,
    this.keyS = false,
    this.keyD = false,

    this.PushBox = false,
    this.MovingBox = false,
    this.pressedD = false,
    this.pressedS = false,
    this.pressedA = false,
    this.pressedW = false,
    this.lastTimeD = new Date(),
    this.lastTimeA = new Date(),
    this.lastTimeS = new Date(),
    this.lastTimeW = new Date(),

    this.sprite_character_rows = 1, // 4 rows and 3 cols in the current sprite sheet
    this.sprite_character_cols = 3, //

    // Character Sprite sheet total length
    this.spriteCharacter_width = 300,
    this.spriteCharacter_height = 90,

    // a single sprite for character with number of cols/ rows
    this.SingleCharSpriteWidth = this.spriteCharacter_width / this.sprite_character_cols,
    this.SingleCharSpriteHeight = this.spriteCharacter_height / this.sprite_character_rows,

    // animation parameter
    this.curFrame = 0,
    this.frameCount = 3,
    this.curBombFrame = 0,
    this.BombframeCount = 5,
    this.BombSrcX = 35,
    this.BombSrcY = 945,
    this.Bombwidth = 500,
    this.BombHeight = 500,
    this.Bomb_cols = 5,
    this.Bomb_rows = 5,
    this.SingleBombSpriteWidth = this.Bombwidth / this.Bomb_cols,
    this.SingleBombSpritHeight = this.BombHeight / this.Bomb_rows,

    this.curDoorFrame = 0,
    this.DoorframeCount = 4,
    this.DoorSrcX = 25,
    this.DoorSrcY = 692,
    this.SingleDoorSpriteWidth = 105,
    this.SingleDoorSpritHeight = 130,

    // x and y corrdinates to render the sprite
    // Key S
    this.charSrcX = 22, // character source coordinate X from Sprite Sheet(key S)
    this.charSrcY = 20, // character source coordinate Y from Sprite Sheet(key S)

    // Key A
    this.charSrcXA = 0, // character source coordinate X from Sprite Sheet(key A)
    this.charSrcYA = 113, // character source coordinate Y from Sprite Sheet(key A)

    this.spriteCharacter_widthA = 315,
    this.spriteCharacter_heightA = 90,
    this.SingleCharSpriteWidthA = this.spriteCharacter_widthA / this.sprite_character_cols,
    this.SingleCharSpriteHeightA = this.spriteCharacter_heightA / 1,

    //key D
    this.charSrcXD = 0,
    this.charSrcYD = 210,
    this.spriteCharacter_heightD = 90,
    this.SingleCharSpriteHeightD = this.spriteCharacter_heightD / this.sprite_character_rows,

    //key W
    this.charSrcXW = 0,
    this.charSrcYW = 304,
    this.spriteCharacter_widthW = 290,
    this.SingleCharSpriteWidthW = this.spriteCharacter_widthW / this.sprite_character_cols,

    //Toast..............................................................
    this.toastElement = document.getElementById('toast'),
    this.countdownInProgress = false,
    // Instructions......................................................

    this.instructionsElement = document.getElementById('instructions');

    // Copyright.........................................................

    this.copyrightElement = document.getElementById('copyright');

    // Score.............................................................

    this.scoreElement = document.getElementById('score'),

    // Sound and music...................................................

    this.soundAndMusicElement = document.getElementById('sound-and-music');

    //Loading screen elements
    this.loadingElement = document.getElementById('loading'),
    this.loadingTitleElement = document.getElementById('loading-title'),
    this.AnimatedGIFElement = document.getElementById('loading-animated-gif'),

    this.bgVelocity = this.STARTING_BACKGROUND_VELOCITY,
    this.BACKGROUND_VELOCITY = 25,

    this.DEFAULT_TOAST_TIME = 3000; // 3 seconds
    this.toast = document.getElementById('toast');

    this.SHORT_DELAY = 50,
    this.TRANSPARENT = 1,
    this.OPAQUE = 10.0,

    // Box parameter
    this.box1 = [200, 200],
    this.box2 = [200, 0],
    this.box3 = [200, 130],
    this.box4 = [275, 250],
    this.box5 = [200, 60],
    this.box6 = [500, 150], 
    this.box7 = [200, 275],
    this.box8 = [10, 125],
    this.box9 = [10, 200],
    this.box10 = [100, 200], // moveable
    this.box11 = [10, 450], // bomb
    this.box12 = [200, 360],
    this.box13 = [10, 275],
    this.box14 = [200, 530],
    this.box15 = [275, 450], // moveable
    this.box16 = [275, 530], // bomb
    this.box17 = [350, 350], // bomb
    this.box18 = [425, 100], 
    this.box19 = [350, 175], // function box
    this.box20 = [425, 175], // bomb
    this.box21 = [500, 530],
    this.box23 = [500, 375], // bomb
    this.box24 = [500, 300],
    this.box25 = [500, 225],
    this.exit = [500, 0], // exit
    this.box26 = [11, 376], // Normal bomb
    this.box27 = [11, 526], // Normal bomb
    this.box28 = [199, 361], // Normal bomb
    this.box29 = [199, 529], // Normal bomb
    this.box30 = [201, 360], // Normal bomb
    this.box31 = [350, 529], // Hard bomb
    this.box32 = [425, 529], // Hard bomb
    this.box33 = [276, 249], // Hard bomb
    this.box34 = [276, 251], // Hard bomb
    this.box35 = [11, 124], // Insane bomb
    this.box36 = [11, 200], // Insane bomb
    this.box37 = [11, 275], // Insane bomb
    this.box38 = [199, 200], // Insane bomb
    this.box39 = [199, 0], // Insane bomb
    this.box40 = [199, 130], // Insane bomb
    this.box41 = [199, 60], // Insane bomb
    this.box42 = [199, 275], // Insane bomb
    this.box43 = [201, 200], // Insane bomb
    this.box44 = [201, 0], // Insane bomb
    this.box45 = [201, 130], // Insane bomb
    this.box46 = [201, 60], // Insane bomb
    this.box47 = [201, 275], // Insane bomb
    this.box48 = [499, 300], // Insane bomb
    this.box49 = [499, 225], // Insane bomb
    this.box50 = [500, 149], // Insane bomb
    this.box51 = [424, 99], // Insane bomb
    this.box52 = [426, 101], // Insane bomb
    this.box53 = [500, 530], // Insane bomb


    // Trigger
    this.bombtrigger = false,
    this.WinTrigger = false,
    this.LoseTrigger = false
};

pushbox.prototype = {
    initializeImages: function () {  //loading image for canvas background
        this.spriteSheet.src = './Assets/Sprites/spritesheet.png';
        this.AnimatedGIFElement.src = './Assets/Images/loading.gif';

        this.spriteSheet.onload = function (e) {
            pushbox.backgroundLoaded(); 
        };

        this.AnimatedGIFElement.onload = function () {
            pushbox.loadingAnimationLoaded();  // Loading page
        };
    },

    //Animation
    animate: function (now) {
        
        if (pushbox.paused) {  //Pause  
            setTimeout(function () {
                requestAnimationFrame(pushbox.animate);
            }, pushbox.PAUSE_CHECK_INTERVAL);
        }
        else {  // Game loop
            pushbox.TimeScore = now;
            pushbox.fps = pushbox.calculateFps(now);
            pushbox.draw(now);                      // Draws the current animation frame
            lastAnimationFrameTime = now;
            requestAnimationFrame(pushbox.animate); // Keep the animation going
            pushbox.drawDoorAnimation();
            pushbox.drawBox();
            pushbox.drawCharacter();
            pushbox.PlayerUI();
        }
    },

    startGame: function () {
        //pushbox.draw();
        this.revealGame();  //display game
        this.revealInitialToast();  //display text when start the game
        requestAnimationFrame(pushbox.animate);
    },

    draw: function (now) {   //call for every animation frame
        pushbox.drawBackground();
    },

    drawBackground: function () {  // render background
        pushbox.context.drawImage(this.background, 0, 0);
    },

    drawRunner: function () {       //redner Runner
        pushbox.context.drawImage(this.runnerImage, 0, 0);
    },

    calculateFps: function (now) {
        var fps = 1 / (now - this.lastAnimationFrameTime) * 1000;

        if (now - this.lastFpsUpdateTime > 1000) {
            //Once per second
            this.lastFpsUpdateTime = now;
            this.fpsElement.innerHTML = fps.toFixed(0) + ' fps';
        }
        this.lastAnimationFrameTime = now;
        return fps;
    },

    togglePaused: function () {  //pause function
        var now = +new Date();  //freeze the game to ensure it resume where it left off
        this.paused = !this.paused;
        if (this.paused) {
            this.pauseStartTime = now;
            this.pauseText.innerHTML = "PAUSED";
        }
        else {
            this.lastAnimationFrameTime += (now - this.pauseStartTime);
            this.pauseText.innerHTML = "";
        }
        
        // pushbox store the time of the last animation frame in its lastanimationframetime, until paused for the first time,
    }, //the property's value reflect the actual time of the last animation frame. Sometimes, when player resume a paused game, that value deviates from reality  

    backgroundLoaded: function () {  // Loading Screen function
        var Loading_Screen_Transition_Duration = 5000; // Loading time

        this.fadeOutElements(this.loadingElement, Loading_Screen_Transition_Duration);

        setTimeout(function () {
            pushbox.startGame();
            pushbox.gameStarted = true;
        }, Loading_Screen_Transition_Duration);
    },

    loadingAnimationLoaded: function () {  // loading animation with fadeIn
        if (!this.gameStarted) {
            this.fadeInElements(this.AnimatedGIFElement, this.loadingTitleElement);
        }
    },

    fadeInElements: function () {
        var args = arguments;

        for (var i = 0; i < args.length; ++i) {
            args[i].style.display = 'block';
        }

        setTimeout(function () {
            for (var i = 0; i < args.length; ++i) {
                args[i].style.opacity = pushbox.OPAQUE;
            }
        }, this.SHORT_DELAY);
    },

    fadeOutElements: function () {
        var args = arguments,
            fadeDuration = args[args.length - 1]; // Last argument

        for (var i = 0; i < args.length - 1; ++i) {
            args[i].style.opacity = this.TRANSPARENT;
        }

        setTimeout(function () {
            for (var i = 0; i < args.length - 1; ++i) {
                args[i].style.display = 'none';
            }
        }, fadeDuration);
    },

    hideToast: function () {  // hide text
        var TOAST_TRANSITION_DURATION = 450;

        this.fadeOutElements(this.toastElement,
            TOAST_TRANSITION_DURATION);
    },

    startToastTransition: function (text, duration) {
        this.toastElement.innerHTML = text;
        this.fadeInElements(this.toastElement);
    },

    revealToast: function (text, duration) {  // display a string in the toast div for a particular duration
        var DEFAULT_TOAST_DISPLAY_DURATION = 1000;

        duration = duration || DEFAULT_TOAST_DISPLAY_DURATION;

        this.startToastTransition(text, duration); // start css transition

        setTimeout(function (e) { //set a count time and run the code
            pushbox.hideToast();
        }, duration);
    },

    revealCanvas: function () { // display canvas
        this.fadeInElements(this.canvas);
    },

    revealTopChrome: function () { // display fps
        this.fadeInElements(this.fpsElement);
    },

    revealTopChromeDimmed: function () {
        var DIM = 0.25;

        this.fpsElement.style.display = 'block';

        setTimeout(function () {
            pushbox.fpsElement.style.opacity = DIM;
        }, this.SHORT_DELAY);
    },

    revealBottomChrome: function () {
        this.fadeInElements(this.soundAndMusicElement);
    },

    revealGame: function () {
        var DIM_CONTROLS_DELAY = 5000; // Tightly coupled to CSS

        this.revealTopChromeDimmed();
        this.revealCanvas();
        this.revealBottomChrome();

        setTimeout(function () {
            pushbox.dimControls();
            pushbox.revealTopChrome();
        }, DIM_CONTROLS_DELAY);
    },

    revealInitialToast: function () {
        var INITIAL_TOAST_DELAY = 1500,
            INITIAL_TOAST_DURATION = 3000;

        setTimeout(function () {
            pushbox.revealToast('Push box and go to the Exit! ', INITIAL_TOAST_DURATION);
        }, INITIAL_TOAST_DELAY);
    },

    dimControls: function () {
        FINAL_OPACITY = 0.5;

        pushbox.soundAndMusicElement.style.opacity = FINAL_OPACITY;
    },

    drawBackground: function () { // draw the background
        pushbox.context.drawImage(this.spriteSheet, 606, 4, 1500, 1200, 0, 0, 1500, 1200); 
    },

    drawBox: function () { // draw all boxes on the canvas
        pushbox.context.drawImage(this.spriteSheet, 103, 425, 75, 75, this.box1[0], this.box1[1], 75, 75);
        pushbox.context.drawImage(this.spriteSheet, 103, 425, 75, 75, this.box2[0], this.box2[1], 75, 75);
        pushbox.context.drawImage(this.spriteSheet, 103, 425, 75, 75, this.box3[0], this.box3[1], 75, 75);
        pushbox.context.drawImage(this.spriteSheet, 103, 425, 75, 75, this.box4[0], this.box4[1], 75, 75);
        pushbox.context.drawImage(this.spriteSheet, 103, 425, 75, 75, this.box5[0], this.box5[1], 75, 75);
        pushbox.context.drawImage(this.spriteSheet, 103, 425, 75, 75, this.box6[0], this.box6[1], 75, 75);
        pushbox.context.drawImage(this.spriteSheet, 103, 425, 75, 75, this.box7[0], this.box7[1], 75, 75);
        pushbox.context.drawImage(this.spriteSheet, 103, 425, 75, 75, this.box8[0], this.box8[1], 75, 75);
        pushbox.context.drawImage(this.spriteSheet, 103, 425, 75, 75, this.box9[0], this.box9[1], 75, 75);
        pushbox.context.drawImage(this.spriteSheet, 22, 425, 75, 75, this.box10[0], this.box10[1], 75, 75); // moveable
        pushbox.context.drawImage(this.spriteSheet, 188, 425, 75, 75, this.box11[0], this.box11[1], 75, 75); // bomb
        pushbox.context.drawImage(this.spriteSheet, 103, 425, 75, 75, this.box12[0], this.box12[1], 75, 75);
        pushbox.context.drawImage(this.spriteSheet, 103, 425, 75, 75, this.box13[0], this.box13[1], 75, 75);
        pushbox.context.drawImage(this.spriteSheet, 103, 425, 75, 75, this.box14[0], this.box14[1], 75, 75);
        pushbox.context.drawImage(this.spriteSheet, 22, 425, 75, 75, this.box15[0], this.box15[1], 75, 75); // moveable
        pushbox.context.drawImage(this.spriteSheet, 188, 425, 75, 75, this.box16[0], this.box16[1], 75, 75); // bomb
        pushbox.context.drawImage(this.spriteSheet, 63, 503, 75, 75, this.box17[0], this.box17[1], 75, 75); // bomb
        pushbox.context.drawImage(this.spriteSheet, 103, 425, 75, 75, this.box18[0], this.box18[1], 75, 75);
        pushbox.context.drawImage(this.spriteSheet, 22, 425, 75, 75, this.box19[0], this.box19[1], 75, 75); // function box
        pushbox.context.drawImage(this.spriteSheet, 63, 503, 75, 75, this.box20[0], this.box20[1], 75, 75); // bomb
        pushbox.context.drawImage(this.spriteSheet, 103, 425, 75, 75, this.box21[0], this.box21[1], 75, 75);
        pushbox.context.drawImage(this.spriteSheet, 188, 425, 75, 75, this.box23[0], this.box23[1], 75, 75); // bomb
        pushbox.context.drawImage(this.spriteSheet, 103, 425, 75, 75, this.box24[0], this.box24[1], 75, 75);
        pushbox.context.drawImage(this.spriteSheet, 103, 425, 75, 75, this.box25[0], this.box25[1], 75, 75);
        if(localStorage.getItem("level") == 2){  // draw extra boxes if game difficulty is normal
            pushbox.context.drawImage(this.spriteSheet, 188, 425, 75, 75, this.box26[0], this.box26[1], 75, 75); // Normal bomb
            pushbox.context.drawImage(this.spriteSheet, 188, 425, 75, 75, this.box27[0], this.box27[1], 75, 75); // Normal bomb
            pushbox.context.drawImage(this.spriteSheet, 188, 425, 75, 75, this.box28[0], this.box28[1], 75, 75); // Normal bomb
            pushbox.context.drawImage(this.spriteSheet, 188, 425, 75, 75, this.box29[0], this.box29[1], 75, 75); // Normal bomb
            pushbox.context.drawImage(this.spriteSheet, 188, 425, 75, 75, this.box30[0], this.box30[1], 75, 75); // Normal bomb
        }
        if(localStorage.getItem("level") == 3){ // draw extra boxes if game difficulty is hard
            pushbox.context.drawImage(this.spriteSheet, 188, 425, 75, 75, this.box26[0], this.box26[1], 75, 75); // Normal bomb
            pushbox.context.drawImage(this.spriteSheet, 188, 425, 75, 75, this.box27[0], this.box27[1], 75, 75); // Normal bomb
            pushbox.context.drawImage(this.spriteSheet, 188, 425, 75, 75, this.box28[0], this.box28[1], 75, 75); // Normal bomb
            pushbox.context.drawImage(this.spriteSheet, 188, 425, 75, 75, this.box29[0], this.box29[1], 75, 75); // Normal bomb
            pushbox.context.drawImage(this.spriteSheet, 188, 425, 75, 75, this.box30[0], this.box30[1], 75, 75); // Normal bomb
            pushbox.context.drawImage(this.spriteSheet, 188, 425, 75, 75, this.box31[0], this.box31[1], 75, 75); // Hard bomb
            pushbox.context.drawImage(this.spriteSheet, 188, 425, 75, 75, this.box32[0], this.box32[1], 75, 75); // Hard bomb
            pushbox.context.drawImage(this.spriteSheet, 188, 425, 75, 75, this.box33[0], this.box33[1], 75, 75); // Hard bomb
            pushbox.context.drawImage(this.spriteSheet, 188, 425, 75, 75, this.box34[0], this.box34[1], 75, 75); // Hard bomb
        }
        if(localStorage.getItem("level") == 4){ // draw extra boxes if game difficulty is insane
            pushbox.context.drawImage(this.spriteSheet, 188, 425, 75, 75, this.box26[0], this.box26[1], 75, 75); // Normal bomb
            pushbox.context.drawImage(this.spriteSheet, 188, 425, 75, 75, this.box27[0], this.box27[1], 75, 75); // Normal bomb
            pushbox.context.drawImage(this.spriteSheet, 188, 425, 75, 75, this.box28[0], this.box28[1], 75, 75); // Normal bomb
            pushbox.context.drawImage(this.spriteSheet, 188, 425, 75, 75, this.box29[0], this.box29[1], 75, 75); // Normal bomb
            pushbox.context.drawImage(this.spriteSheet, 188, 425, 75, 75, this.box30[0], this.box30[1], 75, 75); // Normal bomb
            pushbox.context.drawImage(this.spriteSheet, 188, 425, 75, 75, this.box31[0], this.box31[1], 75, 75); // Hard bomb
            pushbox.context.drawImage(this.spriteSheet, 188, 425, 75, 75, this.box32[0], this.box32[1], 75, 75); // Hard bomb
            pushbox.context.drawImage(this.spriteSheet, 188, 425, 75, 75, this.box33[0], this.box33[1], 75, 75); // Hard bomb
            pushbox.context.drawImage(this.spriteSheet, 188, 425, 75, 75, this.box34[0], this.box34[1], 75, 75); // Hard bomb
            pushbox.context.drawImage(this.spriteSheet, 188, 425, 75, 75, this.box35[0], this.box35[1], 75, 75); // Insane bomb
            pushbox.context.drawImage(this.spriteSheet, 188, 425, 75, 75, this.box36[0], this.box36[1], 75, 75); // Insane bomb
            pushbox.context.drawImage(this.spriteSheet, 188, 425, 75, 75, this.box37[0], this.box37[1], 75, 75); // Insane bomb
            pushbox.context.drawImage(this.spriteSheet, 188, 425, 75, 75, this.box38[0], this.box38[1], 75, 75); // Insane bomb
            pushbox.context.drawImage(this.spriteSheet, 188, 425, 75, 75, this.box39[0], this.box39[1], 75, 75); // Insane bomb
            pushbox.context.drawImage(this.spriteSheet, 188, 425, 75, 75, this.box40[0], this.box40[1], 75, 75); // Insane bomb
            pushbox.context.drawImage(this.spriteSheet, 188, 425, 75, 75, this.box41[0], this.box41[1], 75, 75); // Insane bomb
            pushbox.context.drawImage(this.spriteSheet, 188, 425, 75, 75, this.box42[0], this.box42[1], 75, 75); // Insane bomb
            pushbox.context.drawImage(this.spriteSheet, 188, 425, 75, 75, this.box48[0], this.box48[1], 75, 75); // Insane bomb
            pushbox.context.drawImage(this.spriteSheet, 188, 425, 75, 75, this.box49[0], this.box49[1], 75, 75); // Insane bomb
            pushbox.context.drawImage(this.spriteSheet, 188, 425, 75, 75, this.box50[0], this.box50[1], 75, 75); // Insane bomb
            pushbox.context.drawImage(this.spriteSheet, 188, 425, 75, 75, this.box51[0], this.box51[1], 75, 75); // Insane bomb
            pushbox.context.drawImage(this.spriteSheet, 188, 425, 75, 75, this.box53[0], this.box53[1], 75, 75); // Insane bomb
        }
    },

    // when player press the button down 
    onkeydown: function (event) {    // https://stackoverflow.com/questions/14389864/javascript-html5-making-a-canvas-animation-using-wasd-to-move-around-a-rectang
        var keyCode = event.keyCode;
        switch (keyCode) {
            case pushbox.D_KEY: // when press D down
                pushbox.keyD = true;
                pushbox.keyS = false;
                pushbox.keyA = false;
                pushbox.keyW = false;
                if (!this.pressedD) {
                    this.pressedD = true;
                    pushbox.PushBox = true;
                    pushbox.lastTimeD = new Date();
                }
                break;

            case pushbox.S_KEY: // when press S down
                pushbox.keyS = true;
                pushbox.keyW = false;
                pushbox.keyA = false;
                pushbox.keyD = false;
                if (!this.pressedS) {
                    this.pressedS = true;
                    pushbox.PushBox = true;
                    pushbox.lastTimeS = new Date();
                }
                break;

            case pushbox.A_KEY: // when press A down 
                pushbox.keyA = true;
                pushbox.keyS = false;
                pushbox.keyW = false;
                pushbox.keyD = false;
                pushbox.PushBox = true;
                if (!this.pressedA) {
                    this.pressedA = true;
                    pushbox.PushBox = true;
                    pushbox.lastTimeA = new Date();
                }
                break;

            case pushbox.W_KEY: // when press W down
                pushbox.keyW = true;
                pushbox.keyS = false;
                pushbox.keyA = false;
                pushbox.keyD = false;
                if (!this.pressedW) {
                    this.pressedW = true;
                    pushbox.PushBox = true;
                    pushbox.lastTimeW = new Date();
                }
                break;
        }
    },

    onkeyup: function (event) { // when player release the button
        var keyCode = event.keyCode;
        switch (keyCode) {
            case pushbox.D_KEY: // when release button D
                pushbox.keyD = false;
                this.pressedD = false;
                this.PushBox = false;
                break;

            case pushbox.S_KEY: // when release button S
                pushbox.keyS = false;
                this.pressedS = false;
                this.PushBox = false;
                break;

            case pushbox.A_KEY: // when release button A
                pushbox.keyA = false;
                this.pressedA = false;
                this.PushBox = false;
                break;

            case pushbox.W_KEY: // when release button W
                pushbox.keyW = false;
                this.pressedW = false;
                this.PushBox = false;          
                break;
        }
    },

    drawCharacter: function (now) { // movement
        var nowDate = new Date();
        if (pushbox.keyD == true) { // if button D true, player go right
            pushbox.tickX += 2.5;
            pushbox.drawCharacterAnimationKeyD();
            if(pushbox.tickX + 100 > pushbox.canvas.width){ // Border Collision keyD
                pushbox.tickX = pushbox.canvas.width - 100;
            }
        }
        if (pushbox.keyS == true) { // if button S true, player go down
            pushbox.tickY += 2.5;
            pushbox.drawCharacterAnimation();
            if(pushbox.tickY + 100 > pushbox.canvas.height){
                pushbox.tickY = pushbox.canvas.height - 100; //Border Collision keyS
            }
        }
        if (pushbox.keyA == true) { // if button A true, player go left
            pushbox.tickX -= 2.5;
            pushbox.drawCharacterAnimationKeyA();
            if(pushbox.tickX - 100 < pushbox.canvas.width && pushbox.tickX < 0){ //Border Collision keyA
                pushbox.tickX = 0;
            }
        }
        if (pushbox.keyW == true) { // if button D true, player go up
            pushbox.tickY -= 2.5;
            pushbox.drawCharacterAnimationKeyW();
            if(pushbox.tickY - 100 < pushbox.canvas.height && pushbox.tickY < 0){ //Border Collision keyW
                pushbox.tickY = 0;
            }
        }

        if(pushbox.keyD == false && pushbox.keyW == false && pushbox.keyA == false && pushbox.keyS == false){ // Character face direction since last time pressed
            if(pushbox.lastTimeS.getTime() > this.lastTimeD.getTime() && pushbox.lastTimeS.getTime() > this.lastTimeW.getTime() && pushbox.lastTimeS.getTime() > this.lastTimeA.getTime()){ // key S
                pushbox.context.drawImage(this.spriteSheet, 0, 22, 100, 90, this.tickX, this.tickY, 100, 90); // face to South
                
            }
            else if(pushbox.lastTimeA.getTime() > this.lastTimeD.getTime() && pushbox.lastTimeA.getTime() > this.lastTimeW.getTime() && pushbox.lastTimeA.getTime() > this.lastTimeS.getTime()){ // key A
                pushbox.context.drawImage(this.spriteSheet, 0, 113, 100, 90, this.tickX, this.tickY, 100, 90); // face to left 
                
            }
            else if(pushbox.lastTimeD.getTime() > this.lastTimeA.getTime() && pushbox.lastTimeD.getTime() > this.lastTimeW.getTime() && pushbox.lastTimeD.getTime() > this.lastTimeS.getTime()){ // key D
                pushbox.context.drawImage(this.spriteSheet, 0, 210, 100, 90, this.tickX, this.tickY, 100, 90); // face to right
                
            }
            else if(pushbox.lastTimeW.getTime() > this.lastTimeA.getTime() && pushbox.lastTimeW.getTime() > this.lastTimeS.getTime() && pushbox.lastTimeW.getTime() > this.lastTimeD.getTime()){ // key W
                pushbox.context.drawImage(this.spriteSheet, 0, 304, 100, 90, this.tickX, this.tickY, 100, 90); // face to north
            }else{
                pushbox.context.drawImage(this.spriteSheet, 0, 210, 100, 90, this.tickX, this.tickY, 100, 90); 
            }
        }

        if(pushbox.keyW == true){ // if pressed button W, do collision for button W
            pushbox.BoxCollisionW(this.box1[0], this.box1[1]);
            pushbox.BoxCollisionW(this.box2[0], this.box2[1]);
            pushbox.BoxCollisionW(this.box3[0], this.box3[1]);
            pushbox.BoxCollisionW(this.box4[0], this.box4[1]);
            pushbox.BoxCollisionW(this.box5[0], this.box5[1]);
            pushbox.BoxCollisionW(this.box6[0], this.box6[1]);
            pushbox.BoxCollisionW(this.box7[0], this.box7[1]);
            pushbox.BoxCollisionW(this.box8[0], this.box8[1]);
            pushbox.BoxCollisionW(this.box9[0], this.box9[1]);
            pushbox.BoxCollisionW(this.box10[0], this.box10[1]);
            pushbox.BoxCollisionW(this.box12[0], this.box12[1]);
            pushbox.BoxCollisionW(this.box13[0], this.box13[1]);
            pushbox.BoxCollisionW(this.box14[0], this.box14[1]);
            pushbox.BoxCollisionW(this.box15[0], this.box15[1]);
            pushbox.BoxCollisionW(this.box18[0], this.box18[1]);
            pushbox.BoxCollisionW(this.box19[0], this.box19[1]);
            pushbox.BoxCollisionW(this.box21[0], this.box21[1]);
            pushbox.BoxCollisionW(this.box24[0], this.box24[1]);
            pushbox.BoxCollisionW(this.box25[0], this.box25[1]);
            pushbox.BombCollisionW(this.box11[0], this.box11[1]);
            pushbox.BombCollisionW(this.box16[0], this.box16[1]);
            pushbox.BombCollisionW(this.box17[0], this.box17[1]);
            pushbox.BombCollisionW(this.box20[0], this.box20[1]);
            pushbox.BombCollisionW(this.box23[0], this.box23[1]);
            if(localStorage.getItem("level") == 2){ // if game difficulty is normal, do collision for button W
                pushbox.BombCollisionW(this.box26[0], this.box26[1]);
                pushbox.BombCollisionW(this.box27[0], this.box27[1]);
                pushbox.BombCollisionW(this.box28[0], this.box28[1]);
                pushbox.BombCollisionW(this.box29[0], this.box29[1]);
            }
            if(localStorage.getItem("level") == 3){ // if game difficulty is hard, do collision for button W
                pushbox.BombCollisionW(this.box26[0], this.box26[1]);
                pushbox.BombCollisionW(this.box27[0], this.box27[1]);
                pushbox.BombCollisionW(this.box28[0], this.box28[1]);
                pushbox.BombCollisionW(this.box29[0], this.box29[1]);
                pushbox.BombCollisionW(this.box33[0], this.box33[1]);
                pushbox.BombCollisionW(this.box34[0], this.box34[1]);
            }
            if(localStorage.getItem("level") == 4){ // if game difficulty is insane, do collision for button W
                pushbox.BombCollisionW(this.box26[0], this.box26[1]);
                pushbox.BombCollisionW(this.box27[0], this.box27[1]);
                pushbox.BombCollisionW(this.box28[0], this.box28[1]);
                pushbox.BombCollisionW(this.box29[0], this.box29[1]);
                pushbox.BombCollisionW(this.box33[0], this.box33[1]);
                pushbox.BombCollisionW(this.box34[0], this.box34[1]);
                pushbox.BombCollisionW(this.box50[0], this.box50[1]);
            }
        }
        if(pushbox.keyS == true) { // if pressed button S, do collision for button S
            pushbox.BoxCollisionS(this.box1[0], this.box1[1]);
            pushbox.BoxCollisionS(this.box2[0], this.box2[1]);
            pushbox.BoxCollisionS(this.box3[0], this.box3[1]);
            pushbox.BoxCollisionS(this.box4[0], this.box4[1]);
            pushbox.BoxCollisionS(this.box5[0], this.box5[1]);
            pushbox.BoxCollisionS(this.box6[0], this.box6[1]);
            pushbox.BoxCollisionS(this.box7[0], this.box7[1]);
            pushbox.BoxCollisionS(this.box8[0], this.box8[1]);
            pushbox.BoxCollisionS(this.box9[0], this.box9[1]);
            pushbox.BoxCollisionS(this.box10[0], this.box10[1]);
            pushbox.BoxCollisionS(this.box12[0], this.box12[1]);
            pushbox.BoxCollisionS(this.box13[0], this.box13[1]);
            pushbox.BoxCollisionS(this.box14[0], this.box14[1]);
            pushbox.BoxCollisionS(this.box15[0], this.box15[1]);
            pushbox.BoxCollisionS(this.box18[0], this.box18[1]);
            pushbox.BoxCollisionS(this.box19[0], this.box19[1]);
            pushbox.BoxCollisionS(this.box21[0], this.box21[1]);
            pushbox.BoxCollisionS(this.box24[0], this.box24[1]);
            pushbox.BoxCollisionS(this.box25[0], this.box25[1]);
            pushbox.BombCollisionS(this.box11[0], this.box11[1]);
            pushbox.BombCollisionS(this.box16[0], this.box16[1]);
            pushbox.BombCollisionS(this.box17[0], this.box17[1]);
            pushbox.BombCollisionS(this.box20[0], this.box20[1]);
            pushbox.BombCollisionS(this.box23[0], this.box23[1]);
            if(localStorage.getItem("level") == 2){ // if game difficulty is normal, do collision for button S
                pushbox.BombCollisionS(this.box26[0], this.box26[1]);
                pushbox.BombCollisionS(this.box27[0], this.box27[1]);
                pushbox.BombCollisionS(this.box28[0], this.box28[1]);
                pushbox.BombCollisionS(this.box29[0], this.box29[1]);
            }
            if(localStorage.getItem("level") == 3){ // if game difficulty is hard, do collision for button S
                pushbox.BombCollisionS(this.box26[0], this.box26[1]);
                pushbox.BombCollisionS(this.box27[0], this.box27[1]);
                pushbox.BombCollisionS(this.box28[0], this.box28[1]);
                pushbox.BombCollisionS(this.box29[0], this.box29[1]);
                pushbox.BombCollisionS(this.box31[0], this.box31[1]);
                pushbox.BombCollisionS(this.box32[0], this.box32[1]);
                pushbox.BombCollisionS(this.box33[0], this.box33[1]);
                pushbox.BombCollisionS(this.box34[0], this.box34[1]);
            }
            if(localStorage.getItem("level") == 4){ // if game difficulty is insane, do collision for button S
                pushbox.BombCollisionS(this.box26[0], this.box26[1]);
                pushbox.BombCollisionS(this.box27[0], this.box27[1]);
                pushbox.BombCollisionS(this.box28[0], this.box28[1]);
                pushbox.BombCollisionS(this.box29[0], this.box29[1]);
                pushbox.BombCollisionS(this.box31[0], this.box31[1]);
                pushbox.BombCollisionS(this.box32[0], this.box32[1]);
                pushbox.BombCollisionS(this.box33[0], this.box33[1]);
                pushbox.BombCollisionS(this.box34[0], this.box34[1]);
                pushbox.BombCollisionS(this.box35[0], this.box35[1]);
                pushbox.BombCollisionS(this.box51[0], this.box51[1]);
            }
        }
        if(pushbox.keyA == true) { // if pressed button A, do collision for button A
            pushbox.BoxCollisionA(this.box1[0], this.box1[1]);
            pushbox.BoxCollisionA(this.box2[0], this.box2[1]);
            pushbox.BoxCollisionA(this.box3[0], this.box3[1]);
            pushbox.BoxCollisionA(this.box4[0], this.box4[1]);
            pushbox.BoxCollisionA(this.box5[0], this.box5[1]);
            pushbox.BoxCollisionA(this.box6[0], this.box6[1]);
            pushbox.BoxCollisionA(this.box7[0], this.box7[1]);
            pushbox.BoxCollisionA(this.box8[0], this.box8[1]);
            pushbox.BoxCollisionA(this.box9[0], this.box9[1]);
            pushbox.BoxCollisionA(this.box10[0], this.box10[1]);
            pushbox.BoxCollisionA(this.box12[0], this.box12[1]);
            pushbox.BoxCollisionA(this.box13[0], this.box13[1]);
            pushbox.BoxCollisionA(this.box14[0], this.box14[1]);
            pushbox.BoxCollisionA(this.box15[0], this.box15[1]);
            pushbox.BoxCollisionA(this.box18[0], this.box18[1]);
            pushbox.BoxCollisionA(this.box19[0], this.box19[1]);
            pushbox.BoxCollisionA(this.box21[0], this.box21[1]);
            pushbox.BoxCollisionA(this.box24[0], this.box24[1]);
            pushbox.BoxCollisionA(this.box25[0], this.box25[1]);
            pushbox.BombCollisionA(this.box11[0], this.box11[1]);
            pushbox.BombCollisionA(this.box16[0], this.box16[1]);
            pushbox.BombCollisionA(this.box17[0], this.box17[1]);
            pushbox.BombCollisionA(this.box20[0], this.box20[1]);
            pushbox.BombCollisionA(this.box23[0], this.box23[1]);
            if(localStorage.getItem("level") == 2){ // if game difficulty is normal, do collision for button A
                pushbox.BombCollisionA(this.box26[0], this.box26[1]);
                pushbox.BombCollisionA(this.box27[0], this.box27[1]);
                pushbox.BombCollisionA(this.box28[0], this.box28[1]);
                pushbox.BombCollisionA(this.box29[0], this.box29[1]);
                pushbox.BombCollisionA(this.box30[0], this.box30[1]);
            }
            if(localStorage.getItem("level") == 3){ // if game difficulty is hard, do collision for button A
                pushbox.BombCollisionA(this.box26[0], this.box26[1]);
                pushbox.BombCollisionA(this.box27[0], this.box27[1]);
                pushbox.BombCollisionA(this.box28[0], this.box28[1]);
                pushbox.BombCollisionA(this.box29[0], this.box29[1]);
                pushbox.BombCollisionA(this.box30[0], this.box30[1]);
                pushbox.BombCollisionA(this.box33[0], this.box33[1]);
                pushbox.BombCollisionA(this.box34[0], this.box34[1]);
            }
            if(localStorage.getItem("level") == 4){ // if game difficulty is insane, do collision for button A
                pushbox.BombCollisionA(this.box26[0], this.box26[1]);
                pushbox.BombCollisionA(this.box27[0], this.box27[1]);
                pushbox.BombCollisionA(this.box28[0], this.box28[1]);
                pushbox.BombCollisionA(this.box29[0], this.box29[1]);
                pushbox.BombCollisionA(this.box30[0], this.box30[1]);
                pushbox.BombCollisionA(this.box33[0], this.box33[1]);
                pushbox.BombCollisionA(this.box34[0], this.box34[1]);
                pushbox.BombCollisionA(this.box35[0], this.box35[1]);
                pushbox.BombCollisionA(this.box36[0], this.box36[1]);
                pushbox.BombCollisionA(this.box37[0], this.box37[1]);
                pushbox.BombCollisionA(this.box43[0], this.box43[1]);
                pushbox.BombCollisionA(this.box44[0], this.box44[1]);
                pushbox.BombCollisionA(this.box45[0], this.box45[1]);
                pushbox.BombCollisionA(this.box46[0], this.box46[1]);
                pushbox.BombCollisionA(this.box47[0], this.box47[1]);
                pushbox.BombCollisionA(this.box52[0], this.box52[1]);
            }
        }
        if(pushbox.keyD == true) { // if pressed button D, do collision for button D
            pushbox.BoxCollisionD(this.box1[0], this.box1[1]);
            pushbox.BoxCollisionD(this.box2[0], this.box2[1]);
            pushbox.BoxCollisionD(this.box3[0], this.box3[1]);
            pushbox.BoxCollisionD(this.box4[0], this.box4[1]);
            pushbox.BoxCollisionD(this.box5[0], this.box5[1]);
            pushbox.BoxCollisionD(this.box6[0], this.box6[1]);
            pushbox.BoxCollisionD(this.box7[0], this.box7[1]);
            pushbox.BoxCollisionD(this.box8[0], this.box8[1]);
            pushbox.BoxCollisionD(this.box9[0], this.box9[1]);
            pushbox.BoxCollisionD(this.box10[0], this.box10[1]);
            pushbox.BoxCollisionD(this.box12[0], this.box12[1]);
            pushbox.BoxCollisionD(this.box13[0], this.box13[1]);
            pushbox.BoxCollisionD(this.box14[0], this.box14[1]);
            pushbox.BoxCollisionD(this.box15[0], this.box15[1]);
            pushbox.BoxCollisionD(this.box18[0], this.box18[1]);
            pushbox.BoxCollisionD(this.box19[0], this.box19[1]);
            pushbox.BoxCollisionD(this.box21[0], this.box21[1]);
            pushbox.BoxCollisionD(this.box24[0], this.box24[1]);
            pushbox.BoxCollisionD(this.box25[0], this.box25[1]);
            pushbox.BombCollisionD(this.box11[0], this.box11[1]);
            pushbox.BombCollisionD(this.box16[0], this.box16[1]);
            pushbox.BombCollisionD(this.box17[0], this.box17[1]);
            pushbox.BombCollisionD(this.box20[0], this.box20[1]);
            pushbox.BombCollisionD(this.box23[0], this.box23[1]);
            if(localStorage.getItem("level") == 2){ // if game difficulty is normal, do collision for button D
                pushbox.BombCollisionD(this.box26[0], this.box26[1]);
                pushbox.BombCollisionD(this.box27[0], this.box27[1]);
                pushbox.BombCollisionD(this.box28[0], this.box28[1]);
                pushbox.BombCollisionD(this.box29[0], this.box29[1]);
            }
            if(localStorage.getItem("level") == 3){ // if game difficulty is hard, do collision for button D
                pushbox.BombCollisionD(this.box26[0], this.box26[1]);
                pushbox.BombCollisionD(this.box27[0], this.box27[1]);
                pushbox.BombCollisionD(this.box28[0], this.box28[1]);
                pushbox.BombCollisionD(this.box29[0], this.box29[1]);
                pushbox.BombCollisionD(this.box33[0], this.box33[1]);
                pushbox.BombCollisionD(this.box34[0], this.box34[1]);
            }
            if(localStorage.getItem("level") == 4){ // if game difficulty is insane, do collision for button D
                pushbox.BombCollisionD(this.box26[0], this.box26[1]);
                pushbox.BombCollisionD(this.box27[0], this.box27[1]);
                pushbox.BombCollisionD(this.box28[0], this.box28[1]);
                pushbox.BombCollisionD(this.box29[0], this.box29[1]);
                pushbox.BombCollisionD(this.box33[0], this.box33[1]);
                pushbox.BombCollisionD(this.box34[0], this.box34[1]);
                pushbox.BombCollisionD(this.box38[0], this.box38[1]);
                pushbox.BombCollisionD(this.box39[0], this.box39[1]);
                pushbox.BombCollisionD(this.box40[0], this.box40[1]);
                pushbox.BombCollisionD(this.box41[0], this.box41[1]);
                pushbox.BombCollisionD(this.box42[0], this.box42[1]);
                pushbox.BombCollisionD(this.box48[0], this.box48[1]);
                pushbox.BombCollisionD(this.box49[0], this.box49[1]);
                pushbox.BombCollisionD(this.box51[0], this.box51[1]);
            }
        }
        if(pushbox.bombtrigger == true){ // game lose
            pushbox.drawBombAnimation(); // play bomb animation
            if(pushbox.LoseTrigger == true){ // if lose
                pushbox.lose.style.transition = 1;
                pushbox.lose.style.display = "inline";
                pushbox.togglePaused(); // stop the game
            }
        }
    },

    UpdateCharacterFrame: function () { // Update character frame for button S
        pushbox.curFrame = (pushbox.curFrame + 0.1) % 3;
        pushbox.charSrcX = Math.floor(pushbox.curFrame) * pushbox.SingleCharSpriteWidth;
    },

    drawCharacterAnimation: function () { // Draw character frame for button S
        pushbox.UpdateCharacterFrame();
        pushbox.context.drawImage(pushbox.spriteSheet, pushbox.charSrcX, pushbox.charSrcY, pushbox.SingleCharSpriteWidth, pushbox.SingleCharSpriteHeight, pushbox.tickX, pushbox.tickY, pushbox.SingleCharSpriteWidth, pushbox.SingleCharSpriteHeight);
    },

    UpdateCharacterFrameKeyA: function () { // Update character frame for button A
        pushbox.curFrame = (pushbox.curFrame + 0.1) % pushbox.frameCount;
        pushbox.charSrcXA = Math.floor(pushbox.curFrame) * pushbox.SingleCharSpriteWidthA;
    },

    drawCharacterAnimationKeyA: function () { // Draw character frame for button A
        pushbox.UpdateCharacterFrameKeyA();
        pushbox.context.drawImage(pushbox.spriteSheet, pushbox.charSrcXA, pushbox.charSrcYA, pushbox.SingleCharSpriteWidthA, pushbox.SingleCharSpriteHeightA, pushbox.tickX, pushbox.tickY, pushbox.SingleCharSpriteWidthA, pushbox.SingleCharSpriteHeightA);
    },

    UpdateCharacterFrameKeyD: function () { // Update character frame for button D
        pushbox.curFrame = (pushbox.curFrame + 0.1) % pushbox.frameCount;
        pushbox.charSrcXD = Math.floor(pushbox.curFrame) * pushbox.SingleCharSpriteWidthA;
    },

    drawCharacterAnimationKeyD: function () { // Draw character frame for button D
        pushbox.UpdateCharacterFrameKeyD();
        pushbox.context.drawImage(pushbox.spriteSheet, pushbox.charSrcXD, pushbox.charSrcYD, pushbox.SingleCharSpriteWidthA, pushbox.SingleCharSpriteHeightD, pushbox.tickX, pushbox.tickY, pushbox.SingleCharSpriteWidthA, pushbox.SingleCharSpriteHeightD);
    },

    UpdateCharacterFrameKeyW: function () { // Update character frame for button W
        pushbox.curFrame = (pushbox.curFrame + 0.1) % pushbox.frameCount;
        pushbox.charSrcXW = Math.floor(pushbox.curFrame) * pushbox.SingleCharSpriteWidthA;
    },

    drawCharacterAnimationKeyW: function () { // Draw character frame for button W
        pushbox.UpdateCharacterFrameKeyW();
        pushbox.context.drawImage(pushbox.spriteSheet, pushbox.charSrcXW, pushbox.charSrcYW, pushbox.SingleCharSpriteWidthA, pushbox.SingleCharSpriteHeightD, pushbox.tickX, pushbox.tickY, pushbox.SingleCharSpriteWidthA, pushbox.SingleCharSpriteHeightD);
    },

    UpdateBombAnimation: function () { // update bomb animation
        pushbox.curBombFrame = (pushbox.curBombFrame + 0.1) % pushbox.BombframeCount;
        pushbox.BombSrcX = (Math.floor(pushbox.curBombFrame) * pushbox.SingleBombSpriteWidth)+40;
        if(pushbox.curBombFrame > 4.9){
            pushbox.BombSrcY = pushbox.BombSrcY + pushbox.SingleBombSpritHeight;

        }
        if(pushbox.curBombFrame > 4.9 && pushbox.BombSrcY == 1545){
            pushbox.LoseTrigger = true; // turn on lose trigger
        }
    },

    drawBombAnimation: function () { // draw bomb animation
        pushbox.UpdateBombAnimation();
        pushbox.context.drawImage(pushbox.spriteSheet, pushbox.BombSrcX, pushbox.BombSrcY , pushbox.SingleBombSpriteWidth, pushbox.SingleBombSpritHeight, pushbox.box11[0], pushbox.box11[1], pushbox.SingleBombSpriteWidth, pushbox.SingleBombSpritHeight);
        pushbox.context.drawImage(pushbox.spriteSheet, pushbox.BombSrcX, pushbox.BombSrcY , pushbox.SingleBombSpriteWidth, pushbox.SingleBombSpritHeight, pushbox.box16[0], pushbox.box16[1], pushbox.SingleBombSpriteWidth, pushbox.SingleBombSpritHeight);
        pushbox.context.drawImage(pushbox.spriteSheet, pushbox.BombSrcX, pushbox.BombSrcY , pushbox.SingleBombSpriteWidth, pushbox.SingleBombSpritHeight, pushbox.box17[0], pushbox.box17[1], pushbox.SingleBombSpriteWidth, pushbox.SingleBombSpritHeight);
        pushbox.context.drawImage(pushbox.spriteSheet, pushbox.BombSrcX, pushbox.BombSrcY , pushbox.SingleBombSpriteWidth, pushbox.SingleBombSpritHeight, pushbox.box20[0], pushbox.box20[1], pushbox.SingleBombSpriteWidth, pushbox.SingleBombSpritHeight);
        pushbox.context.drawImage(pushbox.spriteSheet, pushbox.BombSrcX, pushbox.BombSrcY , pushbox.SingleBombSpriteWidth, pushbox.SingleBombSpritHeight, pushbox.box23[0], pushbox.box23[1], pushbox.SingleBombSpriteWidth, pushbox.SingleBombSpritHeight);
        if(localStorage.getItem("level") == 2){ // draw animation if difficulty is normal
        pushbox.context.drawImage(pushbox.spriteSheet, pushbox.BombSrcX, pushbox.BombSrcY , pushbox.SingleBombSpriteWidth, pushbox.SingleBombSpritHeight, pushbox.box26[0], pushbox.box26[1], pushbox.SingleBombSpriteWidth, pushbox.SingleBombSpritHeight);
        pushbox.context.drawImage(pushbox.spriteSheet, pushbox.BombSrcX, pushbox.BombSrcY , pushbox.SingleBombSpriteWidth, pushbox.SingleBombSpritHeight, pushbox.box27[0], pushbox.box27[1], pushbox.SingleBombSpriteWidth, pushbox.SingleBombSpritHeight);
        pushbox.context.drawImage(pushbox.spriteSheet, pushbox.BombSrcX, pushbox.BombSrcY , pushbox.SingleBombSpriteWidth, pushbox.SingleBombSpritHeight, pushbox.box28[0], pushbox.box28[1], pushbox.SingleBombSpriteWidth, pushbox.SingleBombSpritHeight);
        pushbox.context.drawImage(pushbox.spriteSheet, pushbox.BombSrcX, pushbox.BombSrcY , pushbox.SingleBombSpriteWidth, pushbox.SingleBombSpritHeight, pushbox.box29[0], pushbox.box29[1], pushbox.SingleBombSpriteWidth, pushbox.SingleBombSpritHeight);
        pushbox.context.drawImage(pushbox.spriteSheet, pushbox.BombSrcX, pushbox.BombSrcY , pushbox.SingleBombSpriteWidth, pushbox.SingleBombSpritHeight, pushbox.box30[0], pushbox.box30[1], pushbox.SingleBombSpriteWidth, pushbox.SingleBombSpritHeight);
        }
        if(localStorage.getItem("level") == 3){ // draw animation if difficulty is hard
            pushbox.context.drawImage(pushbox.spriteSheet, pushbox.BombSrcX, pushbox.BombSrcY , pushbox.SingleBombSpriteWidth, pushbox.SingleBombSpritHeight, pushbox.box26[0], pushbox.box26[1], pushbox.SingleBombSpriteWidth, pushbox.SingleBombSpritHeight);
            pushbox.context.drawImage(pushbox.spriteSheet, pushbox.BombSrcX, pushbox.BombSrcY , pushbox.SingleBombSpriteWidth, pushbox.SingleBombSpritHeight, pushbox.box27[0], pushbox.box27[1], pushbox.SingleBombSpriteWidth, pushbox.SingleBombSpritHeight);
            pushbox.context.drawImage(pushbox.spriteSheet, pushbox.BombSrcX, pushbox.BombSrcY , pushbox.SingleBombSpriteWidth, pushbox.SingleBombSpritHeight, pushbox.box28[0], pushbox.box28[1], pushbox.SingleBombSpriteWidth, pushbox.SingleBombSpritHeight);
            pushbox.context.drawImage(pushbox.spriteSheet, pushbox.BombSrcX, pushbox.BombSrcY , pushbox.SingleBombSpriteWidth, pushbox.SingleBombSpritHeight, pushbox.box29[0], pushbox.box29[1], pushbox.SingleBombSpriteWidth, pushbox.SingleBombSpritHeight);
            pushbox.context.drawImage(pushbox.spriteSheet, pushbox.BombSrcX, pushbox.BombSrcY , pushbox.SingleBombSpriteWidth, pushbox.SingleBombSpritHeight, pushbox.box30[0], pushbox.box30[1], pushbox.SingleBombSpriteWidth, pushbox.SingleBombSpritHeight);
            pushbox.context.drawImage(pushbox.spriteSheet, pushbox.BombSrcX, pushbox.BombSrcY , pushbox.SingleBombSpriteWidth, pushbox.SingleBombSpritHeight, pushbox.box31[0], pushbox.box31[1], pushbox.SingleBombSpriteWidth, pushbox.SingleBombSpritHeight);
            pushbox.context.drawImage(pushbox.spriteSheet, pushbox.BombSrcX, pushbox.BombSrcY , pushbox.SingleBombSpriteWidth, pushbox.SingleBombSpritHeight, pushbox.box32[0], pushbox.box32[1], pushbox.SingleBombSpriteWidth, pushbox.SingleBombSpritHeight);
            pushbox.context.drawImage(pushbox.spriteSheet, pushbox.BombSrcX, pushbox.BombSrcY , pushbox.SingleBombSpriteWidth, pushbox.SingleBombSpritHeight, pushbox.box33[0], pushbox.box33[1], pushbox.SingleBombSpriteWidth, pushbox.SingleBombSpritHeight);
        }
        if(localStorage.getItem("level") == 4){ // draw animation if difficulty is insane
            pushbox.context.drawImage(pushbox.spriteSheet, pushbox.BombSrcX, pushbox.BombSrcY , pushbox.SingleBombSpriteWidth, pushbox.SingleBombSpritHeight, pushbox.box26[0], pushbox.box26[1], pushbox.SingleBombSpriteWidth, pushbox.SingleBombSpritHeight);
            pushbox.context.drawImage(pushbox.spriteSheet, pushbox.BombSrcX, pushbox.BombSrcY , pushbox.SingleBombSpriteWidth, pushbox.SingleBombSpritHeight, pushbox.box27[0], pushbox.box27[1], pushbox.SingleBombSpriteWidth, pushbox.SingleBombSpritHeight);
            pushbox.context.drawImage(pushbox.spriteSheet, pushbox.BombSrcX, pushbox.BombSrcY , pushbox.SingleBombSpriteWidth, pushbox.SingleBombSpritHeight, pushbox.box28[0], pushbox.box28[1], pushbox.SingleBombSpriteWidth, pushbox.SingleBombSpritHeight);
            pushbox.context.drawImage(pushbox.spriteSheet, pushbox.BombSrcX, pushbox.BombSrcY , pushbox.SingleBombSpriteWidth, pushbox.SingleBombSpritHeight, pushbox.box29[0], pushbox.box29[1], pushbox.SingleBombSpriteWidth, pushbox.SingleBombSpritHeight);
            pushbox.context.drawImage(pushbox.spriteSheet, pushbox.BombSrcX, pushbox.BombSrcY , pushbox.SingleBombSpriteWidth, pushbox.SingleBombSpritHeight, pushbox.box30[0], pushbox.box30[1], pushbox.SingleBombSpriteWidth, pushbox.SingleBombSpritHeight);
            pushbox.context.drawImage(pushbox.spriteSheet, pushbox.BombSrcX, pushbox.BombSrcY , pushbox.SingleBombSpriteWidth, pushbox.SingleBombSpritHeight, pushbox.box31[0], pushbox.box31[1], pushbox.SingleBombSpriteWidth, pushbox.SingleBombSpritHeight);
            pushbox.context.drawImage(pushbox.spriteSheet, pushbox.BombSrcX, pushbox.BombSrcY , pushbox.SingleBombSpriteWidth, pushbox.SingleBombSpritHeight, pushbox.box32[0], pushbox.box32[1], pushbox.SingleBombSpriteWidth, pushbox.SingleBombSpritHeight);
            pushbox.context.drawImage(pushbox.spriteSheet, pushbox.BombSrcX, pushbox.BombSrcY , pushbox.SingleBombSpriteWidth, pushbox.SingleBombSpritHeight, pushbox.box33[0], pushbox.box33[1], pushbox.SingleBombSpriteWidth, pushbox.SingleBombSpritHeight);
            pushbox.context.drawImage(pushbox.spriteSheet, pushbox.BombSrcX, pushbox.BombSrcY , pushbox.SingleBombSpriteWidth, pushbox.SingleBombSpritHeight, pushbox.box35[0], pushbox.box35[1], pushbox.SingleBombSpriteWidth, pushbox.SingleBombSpritHeight);
            pushbox.context.drawImage(pushbox.spriteSheet, pushbox.BombSrcX, pushbox.BombSrcY , pushbox.SingleBombSpriteWidth, pushbox.SingleBombSpritHeight, pushbox.box36[0], pushbox.box36[1], pushbox.SingleBombSpriteWidth, pushbox.SingleBombSpritHeight);
            pushbox.context.drawImage(pushbox.spriteSheet, pushbox.BombSrcX, pushbox.BombSrcY , pushbox.SingleBombSpriteWidth, pushbox.SingleBombSpritHeight, pushbox.box37[0], pushbox.box37[1], pushbox.SingleBombSpriteWidth, pushbox.SingleBombSpritHeight);
            pushbox.context.drawImage(pushbox.spriteSheet, pushbox.BombSrcX, pushbox.BombSrcY , pushbox.SingleBombSpriteWidth, pushbox.SingleBombSpritHeight, pushbox.box38[0], pushbox.box38[1], pushbox.SingleBombSpriteWidth, pushbox.SingleBombSpritHeight);
            pushbox.context.drawImage(pushbox.spriteSheet, pushbox.BombSrcX, pushbox.BombSrcY , pushbox.SingleBombSpriteWidth, pushbox.SingleBombSpritHeight, pushbox.box39[0], pushbox.box39[1], pushbox.SingleBombSpriteWidth, pushbox.SingleBombSpritHeight);
            pushbox.context.drawImage(pushbox.spriteSheet, pushbox.BombSrcX, pushbox.BombSrcY , pushbox.SingleBombSpriteWidth, pushbox.SingleBombSpritHeight, pushbox.box40[0], pushbox.box40[1], pushbox.SingleBombSpriteWidth, pushbox.SingleBombSpritHeight);
            pushbox.context.drawImage(pushbox.spriteSheet, pushbox.BombSrcX, pushbox.BombSrcY , pushbox.SingleBombSpriteWidth, pushbox.SingleBombSpritHeight, pushbox.box41[0], pushbox.box41[1], pushbox.SingleBombSpriteWidth, pushbox.SingleBombSpritHeight);
            pushbox.context.drawImage(pushbox.spriteSheet, pushbox.BombSrcX, pushbox.BombSrcY , pushbox.SingleBombSpriteWidth, pushbox.SingleBombSpritHeight, pushbox.box42[0], pushbox.box42[1], pushbox.SingleBombSpriteWidth, pushbox.SingleBombSpritHeight);
            pushbox.context.drawImage(pushbox.spriteSheet, pushbox.BombSrcX, pushbox.BombSrcY , pushbox.SingleBombSpriteWidth, pushbox.SingleBombSpritHeight, pushbox.box48[0], pushbox.box48[1], pushbox.SingleBombSpriteWidth, pushbox.SingleBombSpritHeight);
            pushbox.context.drawImage(pushbox.spriteSheet, pushbox.BombSrcX, pushbox.BombSrcY , pushbox.SingleBombSpriteWidth, pushbox.SingleBombSpritHeight, pushbox.box49[0], pushbox.box49[1], pushbox.SingleBombSpriteWidth, pushbox.SingleBombSpritHeight);
            pushbox.context.drawImage(pushbox.spriteSheet, pushbox.BombSrcX, pushbox.BombSrcY , pushbox.SingleBombSpriteWidth, pushbox.SingleBombSpritHeight, pushbox.box50[0], pushbox.box50[1], pushbox.SingleBombSpriteWidth, pushbox.SingleBombSpritHeight);
            pushbox.context.drawImage(pushbox.spriteSheet, pushbox.BombSrcX, pushbox.BombSrcY , pushbox.SingleBombSpriteWidth, pushbox.SingleBombSpritHeight, pushbox.box51[0], pushbox.box51[1], pushbox.SingleBombSpriteWidth, pushbox.SingleBombSpritHeight);
            pushbox.context.drawImage(pushbox.spriteSheet, pushbox.BombSrcX, pushbox.BombSrcY , pushbox.SingleBombSpriteWidth, pushbox.SingleBombSpritHeight, pushbox.box53[0], pushbox.box53[1], pushbox.SingleBombSpriteWidth, pushbox.SingleBombSpritHeight);
        }
    },

    UpdateDoorAnimation: function () { // update door animation
        if(pushbox.tickX == 500 && pushbox.WinTrigger == false){
            pushbox.curDoorFrame = (pushbox.curDoorFrame + 0.05) % pushbox.DoorframeCount;
            pushbox.DoorSrcX = (Math.floor(pushbox.curDoorFrame) * pushbox.SingleDoorSpriteWidth);  
            if(pushbox.curDoorFrame > 3.99){ // Game win

                var username = localStorage.getItem("user");
                var playerdata = [];
                var data = localStorage.getItem("playerdata");
                playerdata.push(data + "<br>");
                pushbox.TimeScore = pushbox.TimeScore/1000;
                pushbox.win.style.transition = 1;
                pushbox.win.style.display = "inline";
                pushbox.WinTrigger == true;
                pushbox.CurrentScore.innerHTML = 'Your Score: ' + pushbox.TimeScore + ' Seconds';
                playerdata.push(username, pushbox.TimeScore);
                localStorage.setItem("score", pushbox.TimeScore); // store score data (https://www.w3schools.com/html/html5_webstorage.asp)
                localStorage.setItem("username", username); // store username
                localStorage.setItem("playerdata", playerdata); // store score and username, it will be used in the score board
                if(localStorage.getItem("bestscore") == null){ // if best score is null, set a new best score
                    localStorage.setItem("bestscore", pushbox.TimeScore);
                    localStorage.setItem("bestuser", username);
                    document.getElementById("Bestscore").innerHTML = 'Best Score: ' + localStorage.getItem("bestscore") + ' Seconds';
                }
                if (localStorage.getItem("bestscore") != null && localStorage.getItem("bestscore") > pushbox.TimeScore) { // if new score is better than best score, update score
                    document.getElementById("Bestscore").innerHTML = 'Best Score: ' + localStorage.getItem("bestscore") + ' Seconds';
                    // Store
                    localStorage.setItem("bestscore", pushbox.TimeScore);
                    localStorage.setItem("bestuser", username);
                }
                if(localStorage.getItem("bestscore") != null && localStorage.getItem("bestscore") < pushbox.TimeScore){ // if new score is not better than , do not update
                    document.getElementById("Bestscore").innerHTML = 'Best Score: ' + localStorage.getItem("bestscore") + ' Seconds';
                }
                if(localStorage.getItem("bestscore") != null && localStorage.getItem("bestscore") == pushbox.TimeScore){ // if new score is equal to best score, do nothing
                    document.getElementById("Bestscore").innerHTML = 'Best Score: ' + localStorage.getItem("bestscore") + ' Seconds';
                }
                pushbox.togglePaused(); // stop the game
            }
        }else{
            pushbox.context.drawImage(pushbox.spriteSheet, pushbox.DoorSrcX, pushbox.DoorSrcY , pushbox.SingleDoorSpriteWidth, pushbox.SingleDoorSpritHeight, pushbox.exit[0], pushbox.exit[1], pushbox.SingleDoorSpriteWidth, pushbox.SingleDoorSpritHeight);
        } // draw door
    },
    
    drawDoorAnimation: function () { 
        pushbox.UpdateDoorAnimation();
        pushbox.context.drawImage(pushbox.spriteSheet, pushbox.DoorSrcX, pushbox.DoorSrcY , pushbox.SingleDoorSpriteWidth, pushbox.SingleDoorSpritHeight, pushbox.exit[0], pushbox.exit[1], pushbox.SingleDoorSpriteWidth, pushbox.SingleDoorSpritHeight);
    },

    BoxCollisionW: function(x, y) { // box collision for key W
        if(pushbox.tickX > x - 70 && pushbox.tickX < x + 40 && pushbox.tickY > y - 100 && pushbox.tickY < y && pushbox.tickX++){
            pushbox.tickY = y + 40;
        }
    },

    BoxCollisionA: function(x,y) { // box collision for key A
        if((pushbox.tickY > y - 80 && pushbox.tickY < y + 40) && pushbox.tickX > x - 50 && pushbox.tickX < x){
            pushbox.tickX = x + 60;
        }
    },

    BoxCollisionS: function(x,y) { // box collision for key S
        if((pushbox.tickX > x - 70 && pushbox.tickX < x + 40) && pushbox.tickY > y - 100 && pushbox.tickY < y && pushbox.tickX++){
            pushbox.tickY = y - 100;
        }
        if((pushbox.tickX >= 50 && pushbox.tickX <= 100) && pushbox.tickY == 100){
            this.box10[0] = 100;
            this.box10[1] = 300;
        }
        if((pushbox.tickX >= 50 && pushbox.tickX <= 100) && pushbox.tickY == 200){
            this.box10[0] = 100;
            this.box10[1] = 400;
        }
        if((pushbox.tickX >= 50 && pushbox.tickX <= 100) && pushbox.tickY == 300){
            this.box10[0] = 100;
            this.box10[1] = 520;
        }
    },

    BoxCollisionD: function(x,y) { // box collision for key D
        if((pushbox.tickY > y - 80 && pushbox.tickY < y + 20) && pushbox.tickX > x - 80 && pushbox.tickX < x && pushbox.tickX++){
            pushbox.tickX = x - 80;
        }
        if((pushbox.tickY >= 387.5 && pushbox.tickY <= 430) && pushbox.tickX == 195){
            this.box15[0] = 375;
            this.box15[1] = 450;
        }
        if((pushbox.tickY >= 387.5 && pushbox.tickY <= 430) && pushbox.tickX == 295){
            this.box15[0] = 430;
            this.box15[1] = 450;
        }
        if((pushbox.tickY >= 387.5 && pushbox.tickY <= 430) && pushbox.tickX == 350){
            this.box15[0] = 470;
            this.box15[1] = 450;
        }
    
    },

    BombCollisionW: function(x, y) { // bomb collision for key W
        if(pushbox.tickX > x - 70 && pushbox.tickX < x + 40 && pushbox.tickY > y - 100 && pushbox.tickY < y && pushbox.tickX++){
            pushbox.tickY = y + 40;
            pushbox.bombtrigger = true;
        }
        if((pushbox.tickX >= 300 && pushbox.tickX <= 345) && pushbox.tickY == 180){
            this.box19[0] = 350;
            this.box19[1] = 100;
        }
        if((pushbox.tickX >= 300 && pushbox.tickX <= 345) && pushbox.tickY == 80){
            this.box19[0] = 350;
            this.box19[1] = 25;
        }
        if((pushbox.tickX >= 300 && pushbox.tickX <= 345) && pushbox.tickY == 300){
            this.box10[0] = 350;
            this.box10[1] = 520;
        }
    },

    BombCollisionA: function(x,y) { // bomb collision for key A
        if((pushbox.tickY > y - 80 && pushbox.tickY < y + 40) && pushbox.tickX > x - 50 && pushbox.tickX < x){
            pushbox.tickX = x + 60;
            pushbox.bombtrigger = true;
        }
    },

    BombCollisionS: function(x,y) { // bomb collision for key S
        if((pushbox.tickX > x - 70 && pushbox.tickX < x + 40) && pushbox.tickY > y - 100 && pushbox.tickY < y && pushbox.tickX++){
            pushbox.tickY = y - 100;
            pushbox.bombtrigger = true;
        }
    },

    BombCollisionD: function(x,y) { // bomb collision for key D
        if((pushbox.tickY > y - 80 && pushbox.tickY < y + 20) && pushbox.tickX > x - 80 && pushbox.tickX < x && pushbox.tickX++){
            pushbox.tickX = x - 80;
            pushbox.bombtrigger = true;
        }
    },

    PlayerUI: function(){ // game UI elements
        var topscore = localStorage.getItem("bestscore"),
            topuser = localStorage.getItem("bestuser"),
            user = localStorage.getItem("user"),
            difficulty = localStorage.getItem("difficulty");
        
        pushbox.playerdetails.innerHTML = "Best score: " + topscore + "<br><br>" + "Best Player: " + topuser + "<br><br>" + "Your name: " + user+ "<br><br>" + "Difficulty: " + difficulty;
    }
}

window.addEventListener("blur", function (e) {  // fix windowHasFocus and countdownInProgress flags
    pushbox.windowHasFocus = false;

    if (!pushbox.paused) { // if the game is not paused,
        pushbox.togglePaused(); //pause if not paused
    }
});

window.addEventListener('keydown', function (e) { // pause game by button P
    var key = e.keyCode;

    if (key === pushbox.P_KEY) {
        pushbox.togglePaused();
    }
});

window.onfocus = function (e) { // doesn't work properly, if player activates another window or tab during the countdown,
    // cuz game will restart at the end of the countdown whether window has focus or not
    var originalFont = pushbox.toast.style.fontSize, //restore later
        DIGIT_DISPLAY_DURATION = 1000; //milliseconds

    pushbox.windowHasFocus = true;
    pushbox.countdownInProgress = true;

    if (pushbox.paused) {
        pushbox.toast.style.font = '128px fantasy'; //Large font

        if (pushbox.windowHasFocus && pushbox.countdownInProgress)
            pushbox.revealToast('3', 500); // Display 3 for 1.0 sec

        setTimeout(function (e) {
            if (pushbox.windowHasFocus && pushbox.countdownInProgress)
                pushbox.revealToast('2', 500) // Display 2 for 1.0 sec

            setTimeout(function (e) {
                if (pushbox.windowHasFocus && pushbox.countdownInProgress)
                    pushbox.revealToast('1', 500); // display 1 for 1.0 sec

                setTimeout(function (e) {
                    if (pushbox.windowHasFocus && pushbox.countdownInProgress)
                        pushbox.togglePaused();
                    if (pushbox.windowHasFocus && pushbox.countdownInProgress)
                        pushbox.toast.style.fontSize = originalFont;

                    pushbox.countdownInProgress = false;

                }, DIGIT_DISPLAY_DURATION); // End of '1'
            }, DIGIT_DISPLAY_DURATION); //End of '2'
        }, DIGIT_DISPLAY_DURATION); // End of '3'
    }
};

//Launch Game
var pushbox = new pushbox();
pushbox.initializeImages();

window.addEventListener("keydown", pushbox.onkeydown, false);
window.addEventListener("keyup", pushbox.onkeyup, false);
